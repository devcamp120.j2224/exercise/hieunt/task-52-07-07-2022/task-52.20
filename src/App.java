import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import com.devcamp.j53_basicjava.Order;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order> orderList = new ArrayList<Order>();
        Order order1 = new Order();
        Order order2 = new Order("Lan");
        Order order3 = new Order(3, "Long", 80000);
        Order order4 = new Order(4, "Nam", 75000, new Date(), false, new String[] {"paper", "pen"});
 
        //Add cac order vao orderList
        orderList.add(order1);
        orderList.add(order2);
        orderList.add(order3);
        orderList.add(order4);
        
        //In ra màn hình
        /* gõ code này vào terminal để hiển thị ký tự đặc biệt
        $OutputEncoding = [console]::InputEncoding = [console]::OutputEncoding =
                    New-Object System.Text.UTF8Encoding
        */
        try ( 
            PrintWriter consoleOut = new PrintWriter(new OutputStreamWriter(System.out, StandardCharsets.UTF_8)))
        {
            for(Order order : orderList) {
                System.out.println(order.toString());
            }
        } 
    }
}
